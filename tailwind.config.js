module.exports = {
  mode: "jit",
  purge: {
    content: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  },
  darkMode: "class", // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: ["Nunito Sans, sans-serif"],
      },
      colors: {
        brandGreen: "#eeefff",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
